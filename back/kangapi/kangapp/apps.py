"""Modulo que define apps"""
from django.apps import AppConfig


class KangappConfig(AppConfig):
    """Classe que define apps"""
    name = 'kangapp'
