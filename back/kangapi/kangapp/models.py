# pylint: disable=invalid-name
# pylint: disable-msg=R0902
# pylint: disable-msg=R0913
# pylint: disable-msg=R0903
# pylint: disable-msg=W0105
# pylint: disable-msg=C0303
"""Modulo de modelos"""
from django.db import models

# Create your models here.

class Product():
    """Classe produtos"""
    def __init__(self, id_flight, origin, destination, departureDate, arrivalDate, value, amount, type_flight):
        self.id_product = id_flight
        self.origin = origin
        self.nameProduct = destination
        self.checkin = departureDate
        self.checkout = arrivalDate
        self.price = value
        self.amount = amount
        self.type_product = type_flight

class Package():
    """Classe de pacotes"""
    def __init__(self, flight, hotel):
        self.flight = flight
        self.hotel = hotel

class User(models.Model):
    """Classe de usuarios"""
    name = models.CharField(max_length=40)
    email = models.CharField(max_length=30)
    password = models.CharField(max_length=255)

    class Meta:
        """Classe meta"""
        db_table = 'users'

class Shopping(models.Model):
    """Classe de compras"""
    date_shopping = models.DateTimeField()
    origin = models.CharField(max_length=80)
    nameProduct = models.CharField(max_length=80)
    checkin = models.DateField()
    checkout = models.DateField(null=True, blank=True)
    price = models.FloatField()
    amount = models.IntegerField()
    type_product = models.CharField(max_length=15)
    client = models.ForeignKey(User, on_delete=models.CASCADE)

    class Meta:
        """Classe meta"""
        db_table = 'shopping_order'

class Airport(models.Model):
    """Classe de aeroportos"""
    city = models.CharField(max_length=40)
    country = models.CharField(max_length=40)
    iataCode = models.CharField(max_length=3, primary_key=True)
    fullAddress = models.CharField(max_length=80)
    isBookable = models.BooleanField()
 
    class Meta:
        """Classe meta"""
        db_table = 'airports'

class City(models.Model):
    """Classe de cidades"""
    city = models.CharField(max_length=40)
    lat = models.FloatField()
    lng = models.FloatField()
    country = models.CharField(max_length=40)
    fullAddress = models.CharField(max_length=80)

    class Meta:
        """Classe meta"""
        db_table = 'cities'

'''
class Flight(models.Model):
  id_flight = models.IntegerField('id', primary_key=True)
  type_flight = models.CharField('type', max_length=30)
  source = models.CharField(max_length=30)
  instantTicketingRequired = models.BooleanField()
  nonHomogeneous = models.BooleanField()
  oneWay = models.BooleanField()
  lastTicketingDate = models.DateField()
  numberOfBookableSeats = models.IntegerField()

class Departure(models.Model):
  iataCode = models.CharField(max_length=3)
  terminal = models.IntegerField()
  at = models.DateTimeField()

class Arrival(models.Model):
  iataCode = models.CharField(max_length=3)
  terminal = models.IntegerField()
  at = models.DateTimeField()

class Aircraft(models.Model):
  code = models.IntegerField(primary_key=True)

class Operating(models.Model):
  carrierCode = models.CharField(max_length=2, primary_key=True)

class Itinerary(models.Model):
  duration = models.CharField(max_length=7)
  flight = models.ForeignKey(Flight, on_delete=models.CASCADE)

class Segment(models.Model):
  id_itinerary = models.IntegerField('id', primary_key=True)
  duration = models.CharField(max_length=7)
  carrierCode = models.CharField(max_length=2)
  number = models.IntegerField()
  numberOfStops = models.IntegerField()
  blacklistedInEU = models.BooleanField()
  departure = models.OneToOneField(Departure, on_delete=models.CASCADE)
  arrival = models.OneToOneField(Arrival, on_delete=models.CASCADE)
  aircraft = models.ForeignKey(Aircraft, on_delete=models.CASCADE)
  itinerary = models.ForeignKey(Itinerary, on_delete=models.CASCADE)
  operating = models.OneToOneField(Operating, on_delete=models.CASCADE)
  
class Price(models.Model):
  currency = models.CharField(max_length=3)
  total = models.FloatField()
  base = models.FloatField()
  grandTotal = models.FloatField()

class Fee(models.Model):
  type_fee = models.CharField('type', max_length=30)
  amount = models.FloatField()
  price = models.ForeignKey(Price, on_delete=models.CASCADE)

class FareType(models.Model): 
  fareType = models.CharField(max_length=30, primary_key=True)

class ValidatingAirlineCodes(models.Model):
  validatingAirlineCodes = models.CharField(max_length=2, primary_key=True)

class PrincingOptions(models.Model):
  includedCheckedBags = models.BooleanField()
  fareType = models.ForeignKey(FareType, on_delete=models.CASCADE)
  validatingAirlineCodes = models.ForeignKey(ValidatingAirlineCodes, on_delete=models.CASCADE)

class TravelerPrincings(models.Model):
  travelerId = models.IntegerField()
  fareOption = models.CharField(max_length=30)
  travelerType = models.CharField(max_length=30)
  price = models.ForeignKey(Price, on_delete=models.CASCADE)

class IncludedCheckedBags(models.Model):
  weight = models.IntegerField()
  quantity = models.IntegerField()
  weightUnit = models.CharField(max_length=5)
 
class FareDetailBySegment(models.Model):
  segmentId = models.IntegerField()
  cabin = models.CharField(max_length=30)
  fareBasis = models.CharField(max_length=30)
  class_segment = models.CharField(max_length=1)
  includedCheckedBags = models.OneToOneField(IncludedCheckedBags, on_delete=models.CASCADE)
'''
'''
 {
      "type": "flight-offer",
      "id": "1",
      "source": "GDS",
      "instantTicketingRequired": false,
      "nonHomogeneous": false,
      "oneWay": false,
      "lastTicketingDate": "2020-08-01",
      "numberOfBookableSeats": 9,
      "itineraries": [
        {
          "duration": "PT9H20M",
          "segments": [
            {
              "departure": {
                "iataCode": "SYD",
                "terminal": "1",
                "at": "2020-08-01T10:00:00"
              },
              "arrival": {
                "iataCode": "BKK",
                "at": "2020-08-01T16:20:00"
              },
              "carrierCode": "TG",
              "number": "476",
              "aircraft": {
                "code": "747"
              },
              "operating": {
                "carrierCode": "TG"
              },
              "duration": "PT9H20M",
              "id": "1",
              "numberOfStops": 0,
              "blacklistedInEU": false
            }
          ]
        },
        {
          "duration": "PT9H",
          "segments": [
            {
              "departure": {
                "iataCode": "BKK",
                "at": "2020-08-05T19:20:00"
              },
              "arrival": {
                "iataCode": "SYD",
                "terminal": "1",
                "at": "2020-08-06T07:20:00"
              },
              "carrierCode": "TG",
              "number": "475",
              "aircraft": {
                "code": "747"
              },
              "operating": {
                "carrierCode": "TG"
              },
              "duration": "PT9H",
              "id": "2",
              "numberOfStops": 0,
              "blacklistedInEU": false
            }
          ]
        }
      ],
      "price": {
        "currency": "EUR",
        "total": "1590.78",
        "base": "1390.00",
        "fees": [
          {
            "amount": "0.00",
            "type": "SUPPLIER"
          },
          {
            "amount": "0.00",
            "type": "TICKETING"
          }
        ],
        "grandTotal": "1590.78"
      },
      "pricingOptions": {
        "fareType": [
          "PUBLISHED"
        ],
        "includedCheckedBagsOnly": true
      },
      "validatingAirlineCodes": [
        "TG"
      ],
      "travelerPricings": [
        {
          "travelerId": "1",
          "fareOption": "STANDARD",
          "travelerType": "ADULT",
          "price": {
            "currency": "EUR",
            "total": "795.39",
            "base": "695.00"
          },
          "fareDetailsBySegment": [
            {
              "segmentId": "1",
              "cabin": "ECONOMY",
              "fareBasis": "W1YRTTG",
              "class": "W",
              "includedCheckedBags": {
                "weight": 30,
                "weightUnit": "KG"
              }
            },
            {
              "segmentId": "2",
              "cabin": "ECONOMY",
              "fareBasis": "W1YRTTG",
              "class": "W",
              "includedCheckedBags": {
                "weight": 30,
                "weightUnit": "KG"
              }
            }
          ]
        },
        {
          "travelerId": "2",
          "fareOption": "STANDARD",
          "travelerType": "ADULT",
          "price": {
            "currency": "EUR",
            "total": "795.39",
            "base": "695.00"
          },
          "fareDetailsBySegment": [
            {
              "segmentId": "1",
              "cabin": "ECONOMY",
              "fareBasis": "W1YRTTG",
              "class": "W",
              "includedCheckedBags": {
                "weight": 30,
                "weightUnit": "KG"
              }
            },
            {
              "segmentId": "2",
              "cabin": "ECONOMY",
              "fareBasis": "W1YRTTG",
              "class": "W",
              "includedCheckedBags": {
                "weight": 30,
                "weightUnit": "KG"
              }
            }
          ]
        }
      ]
    }
    '''
