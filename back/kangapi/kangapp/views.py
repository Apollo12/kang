# pylint: disable=invalid-name
# pylint: disable=mixed-indentation
# pylint: disable-msg=W0613
# pylint: disable-msg=R0913
# pylint: disable-msg=W0611
# pylint: disable-msg=R0901
# pylint: disable-msg=E0611
# pylint: disable-msg=E1101
"""Modulo onde obtem os resultados das views"""
from django.shortcuts import render
from django.http import JsonResponse, HttpResponse
from rest_framework import generics, viewsets
from . import services
from .models import User, Shopping, Airport, City
from .serializers import UserSerializer, ShoppingSerializer, AirportSerializer, CitySerializer


# Create your views here.
def teste(request):
	"""View teste"""
	flights_json = services.search_flights('SYD', 'BKK', '2020-10-01', 1, 10000, False)
	return HttpResponse(flights_json, 'application/json')

def get_flights(request, origin, destination, departureDate, adults, maxvalue):
	"""View de voos"""
	flights_json = services.search_flights(origin, destination, departureDate, adults, maxvalue, False)
	return HttpResponse(flights_json, 'application/json')

def get_flights_return(request, origin, destination, departureDate, adults, maxvalue, returnDate):
	"""View de voos retorno"""
	flights_json = services.search_flights(origin, destination, departureDate, adults, maxvalue, True, returnDate)
	return HttpResponse(flights_json, 'application/json')

def get_packages(request, origin, destination, departureDate, returnDate, maxvalue):
	"""View de pacotes"""
	packages_json = services.search_packages(origin, destination, departureDate, returnDate, maxvalue)
	return HttpResponse(packages_json, 'application/json')

class ShoppingViewSet(viewsets.ModelViewSet):
	"""View de compras"""
	queryset = Shopping.objects.all()
	serializer_class = ShoppingSerializer

class UserViewSet(viewsets.ModelViewSet):
	"""View de usuarios"""
	queryset = User.objects.all()
	serializer_class = UserSerializer

class AirportViewSet(viewsets.ModelViewSet):
	"""View de aeroportos"""
	queryset = Airport.objects.all()
	serializer_class = AirportSerializer

class CityViewSet(viewsets.ModelViewSet):
	"""View de cidades"""
	queryset = City.objects.all()
	serializer_class = CitySerializer
