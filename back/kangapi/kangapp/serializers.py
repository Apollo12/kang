# pylint: disable=mixed-indentation
# pylint: disable=abstract-method
# pylint: disable-msg=R0903
# pylint: disable-msg=C0303
"""Modulo serializador"""
from rest_framework import serializers
from .models import User, Shopping, Airport, City

class ProductSerializer(serializers.Serializer):
	"""Serializer para o produto"""
	id_product = serializers.IntegerField()
	origin = serializers.CharField()
	nameProduct = serializers.CharField()
	checkin = serializers.DateField()
	checkout = serializers.DateField()
	price = serializers.FloatField()
	amount = serializers.IntegerField()
	type_product = serializers.CharField()


class UserSerializer(serializers.ModelSerializer):
	"""Serializer para o usuario"""
	class Meta:
		"""Classe meta"""
		model = User
		fields = '__all__'

class ShoppingSerializer(serializers.ModelSerializer):
	"""Serializer para a compra"""
	class Meta:
		"""Classe meta"""
		model = Shopping
		fields = '__all__'

class AirportSerializer(serializers.ModelSerializer):
	"""Serializer para o aeroporto"""
	class Meta:
		"""Classe meta"""
		model = Airport
		fields = '__all__'

class CitySerializer(serializers.ModelSerializer):
	"""Serializer para a cidade"""
	class Meta:
		"""Classe meta"""
		model = City
		fields = ('lat', 'lng', 'fullAddress')
		
