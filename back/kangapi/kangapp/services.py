# pylint: disable=mixed-indentation
# pylint: disable=invalid-name
# pylint: disable-msg=C0326
# pylint: disable-msg=C0325
# pylint: disable-msg=C0303
# pylint: disable-msg=C0410
# pylint: disable-msg=E0611
# pylint: disable-msg=R0914
# pylint: disable-msg=W0622
# pylint: disable-msg=R0913
# pylint: disable-msg=W0105
# pylint: disable-msg=W0612
# pylint: disable-msg=W0611
# pylint: disable-msg=C0411
"""Modulo dos servicos"""
import json, requests
from kangapi import settings
from amadeus import Client, ResponseError, Location
from .models import Product, Package
from .serializers import ProductSerializer
from rest_framework.renderers import JSONRenderer

amadeus = Client(client_id=settings.AMADEUS_KEY, client_secret=settings.AMADEUS_SECRET)

def get_flights_amadeus(originLocationCode, destinationLocationCode, departureDate, adults):
	"""Obtem voos"""
	try:
		response = amadeus.shopping.flight_offers_search.get(
		    originLocationCode = originLocationCode,
		    destinationLocationCode = destinationLocationCode,
		    departureDate = departureDate,
		    #returnDate = returnDate,
		    adults = adults,
		    currencyCode = "USD"
		    )
		return response
	except ResponseError as error:
		return error

def get_flights_amadeus_idavolta(originLocationCode, destinationLocationCode, departureDate, adults, returnDate):
	"""Obtem voos ida e volta"""
	try:
		response = amadeus.shopping.flight_offers_search.get(
		    originLocationCode = originLocationCode,
		    destinationLocationCode = destinationLocationCode,
		    departureDate = departureDate,
		    returnDate = returnDate,
		    adults = adults,
		    currencyCode = "USD"
		    )
		return response
	except ResponseError as error:
		return error

def create_flights(response, maxvalue, isVolta):
	"""Cria o objeto de voo"""
	if len(response.data) == 0:
		return []
	flights_list = []
	departureDate = response.data[0]['itineraries'][0]['segments'][0]['departure']['at'][0:10]
	origin = response.data[0]['itineraries'][0]['segments'][0]['departure']['iataCode']
	amount = len(response.data[0]['travelerPricings'])

	for f in response.data:
		id_flight = f['id']
		valueTotal = float(f['price']['total'])
		value = valueTotal/amount
		destination = f['itineraries'][0]['segments'][len(f['itineraries'][0]['segments'])-1]['arrival']['iataCode']
		if(isVolta):
			aux = f['itineraries'][len(f['itineraries'])-1]['segments'][len(f['itineraries'][len(f['itineraries'])-1]['segments'])-1]
			arrivalDate = aux['departure']['at'][0:10]
		else:
			arrivalDate = None
		tipo = 'Flight'
		flight = Product(id_flight, origin, destination, departureDate, arrivalDate, value, amount, tipo)
		if (value <= float(maxvalue)):
			flights_list.append(flight)	
	return flights_list

def list_toJson(list):
	"""Cria json do voo"""
	serializer = ProductSerializer(list, many=True)
	products_json = JSONRenderer().render(serializer.data)
	return products_json

def search_flights(originLocationCode, destinationLocationCode, departureDate, adults, maxvalue, isVolta, returnDate=None):
	"""Busca voo"""
	if (isVolta):
		response = get_flights_amadeus_idavolta(originLocationCode, destinationLocationCode, departureDate, adults, returnDate)
		flights_list = create_flights(response, maxvalue, True)
	else:
		response = get_flights_amadeus(originLocationCode, destinationLocationCode, departureDate, adults)
		flights_list = create_flights(response, maxvalue, False)
	flights_json = list_toJson(flights_list)
	return flights_json

'''
bloco de código do pacote
'''
def get_hotels(destination, checkin, checkout, adults, price):
	"""Busca hotel"""
	API_URL = 'https://still-totality-278901.rj.r.appspot.com/hotel/consultar?'
	response = requests.get(API_URL + 'cidade=' + destination + '&checkin=' + checkin + '&checkout=' + checkout + '&adulto=' + adults + '&preco=' + price)
	return response

def create_hotels(response):
	"""Cria objeto do hotel"""
	hotels_list = []
	for hotel in response.json():
		h = Product(hotel['id_product'], hotel['origin'], hotel['nameProduct'], hotel['checkin'], hotel['checkout'], hotel['price'], hotel['amount'], hotel['type_product'])
		hotels_list.append(hotel)
	return hotels_list

def package_toJson(packages_list):
	"""Cria json de pacotes"""
	packages_json = "["
	for p in packages_list:
		serializer_flight = ProductSerializer(p.flight)
		serializer_hotel = ProductSerializer(p.hotel)

		flight_json = JSONRenderer().render(serializer_flight.data)
		hotel_json = JSONRenderer().render(serializer_hotel.data)

		packages_json+= '{"flight": ' + (flight_json.decode('utf-8')) + ","
		packages_json+= '"hotel": ' + (hotel_json.decode('utf-8')) + "},"

	packages_json = packages_json[:-1] + "]"
	return packages_json

def create_packages(hotels_list, flights_list):
	"""Cria objeto de pacote"""
	packages_list = []
	package_range = len(hotels_list)

	if (len(hotels_list) > len(flights_list)):
		package_range = len(flights_list)

	for i in range(package_range):
		package = Package(flights_list[i], hotels_list[i])
		packages_list.append(package)

	return packages_list

def search_packages(origin, destination, departureDate, returnDate, maxvalue):
	"""Busca por pacote"""
	response_hotels = get_hotels(destination, departureDate, returnDate, "1", maxvalue)
	hotels_list = create_hotels(response_hotels)

	response_flights = get_flights_amadeus_idavolta(origin, destination, departureDate, 1, returnDate)
	flights_list = create_flights(response_flights, maxvalue, True)

	packages_list = create_packages(hotels_list, flights_list)
	packages_json = package_toJson(packages_list)

	return packages_json
