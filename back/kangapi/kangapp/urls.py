# pylint: disable=unused-import
# pylint: disable=invalid-name
# pylint: disable-msg=E0401
"""Programa urls, define os padrões de URL"""
from django.urls import path, include
from django.conf.urls import url

from rest_framework.routers import DefaultRouter
from kangapp.views import UserViewSet, ShoppingViewSet, AirportViewSet, CityViewSet
from . import views

router = DefaultRouter()
router.register('users', views.UserViewSet)
router.register('shoppings', views.ShoppingViewSet)
router.register('airports', views.AirportViewSet)
router.register('cities', views.CityViewSet)

urlpatterns = [
    path('', views.teste),
    path('flight_search/<origin>/<destination>/<departureDate>/<adults>/<maxvalue>', views.get_flights),
    path('flight_search/<origin>/<destination>/<departureDate>/<adults>/<maxvalue>/<returnDate>', views.get_flights_return),
    path('package_search/<origin>/<destination>/<departureDate>/<maxvalue>/<returnDate>', views.get_packages),
    path('', include(router.urls)),
]

# exemplo de path - flight_search/SYD/BKK/2020-10-01/1/500
