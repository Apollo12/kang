import { HttpClient, HttpClientModule } from '@angular/common/http';
import { ErrorHandler, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Camera } from '@ionic-native/camera';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { IonicStorageModule, Storage } from '@ionic/storage';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { Settings } from '../providers';
import { MyApp } from './app.component';

import { CartService } from '../services/cart.service';
import { SearchService } from '../services/search.service';
import { UserService } from '../services/user.service';

import { UserProvider } from '../providers/user/user';

import { FlightsProvider } from '../providers/flights/flights';
import { CotacaoProvider } from '../providers/cotacao/cotacao';
import { ShoppingProvider } from '../providers/shopping/shopping';
import { AirportsProvider } from '../providers/airports/airports';
import { hoteisProvider } from '../providers/hotel/hotel';
import { WeatherProvider } from '../providers/weather/weather';
import { CalculatorProvider } from '../providers/calculator/calculator';
import { CitiesProvider } from '../providers/cities/cities';
import { PackagesProvider } from '../providers/packages/packages';

// The translate loader needs to know where to load i18n files
// in Ionic's static asset pipeline.
export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

export function provideSettings(storage: Storage) {
  /**
   * The Settings provider takes a set of default settings for your app.
   *
   * You can add new settings options at any time. Once the settings are saved,
   * these values will not overwrite the saved values (this can be done manually if desired).
   */
  return new Settings(storage, {
    option1: true,
    option2: 'Ionitron J. Framework',
    option3: '3',
    option4: 'Hello'
  });
}

@NgModule({
  declarations: [
    MyApp
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    }),
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp
  ],
  providers: [
    PackagesProvider,
    AirportsProvider,
    ShoppingProvider,
    UserService,
    UserProvider,
    SearchService,
    CartService,
    Camera,
    SplashScreen,
    StatusBar,
    { provide: Settings, useFactory: provideSettings, deps: [Storage] },
    // Keep this to enable Ionic's runtime error handling during development
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    FlightsProvider,
    CotacaoProvider,
    WeatherProvider,
    hoteisProvider,
    CalculatorProvider,
    CitiesProvider,
  ]
})
export class AppModule { }
