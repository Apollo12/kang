import { Injectable } from '@angular/core';
import { Product } from '../models/product';
import { BehaviorSubject } from 'rxjs';


@Injectable()
export class CartService {
  private cart = [];
  private cartItemCount = new BehaviorSubject(0);

  constructor() { }

 
  getCart() {
    return this.cart;
  }

  clearCart() {
    this.cart.splice(0, this.cart.length);
    this.cartItemCount.next(0);
  }
 
  getCartItemCount() {
    return this.cartItemCount;
  }
 
  addProduct(product) {
    let added = false;
    for (let p of this.cart) {
      if ((p.id_product === product.id_product) && (p.nameProduct === product.nameProduct)) {
        p.amount += product.amount;
        added = true;
        break;
      }
    }
    if (!added) {
      product.amount = product.amount;
      this.cart.push({...product});
    }
    this.cartItemCount.next(this.cartItemCount.value + product.amount);
  }

  addOneProduct(product) {
    let added = false;
    for (let p of this.cart) {
      if ((p.id_product === product.id_product) && (p.nameProduct === product.nameProduct)) {
        p.amount += 1;
        added = true;
        break;
      }
    }
    if (!added) {
      product.amount = 1;
      this.cart.push({...product});
    }
    this.cartItemCount.next(this.cartItemCount.value + 1);
  }

 
  decreaseProduct(product) {
    for (let [index, p] of this.cart.entries()) {
      if ((p.id_product === product.id_product)  && (p.nameProduct === product.nameProduct)) {
        p.amount -= 1;
        if (p.amount == 0) {
          this.cart.splice(index, 1);
        }
      }
    }
    this.cartItemCount.next(this.cartItemCount.value - 1);
  }
 
  removeProduct(product) {
    for (let [index, p] of this.cart.entries()) {
      if ((p.id_product === product.id_product) && (p.nameProduct === product.nameProduct)) {
        this.cartItemCount.next(this.cartItemCount.value - p.amount);
        this.cart.splice(index, 1);
      }
    }
  }
}