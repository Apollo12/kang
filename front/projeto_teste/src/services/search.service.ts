import { Injectable } from '@angular/core';
import { AirportsProvider } from '../providers/airports/airports';
import { Airport } from '../models/airport';

@Injectable()
export class SearchService {
	private origin: string;
	private destination: string;
	private departureDate: Date;
	private adults: number;
	private chekInDate: Date;
	private checkoutDate: Date;
	private price: number;
	private maxvalue: number;
	private airports: Airport[];
	private plane_checkbox;
	private returnDate;

	constructor(private airportsProvider: AirportsProvider) {
	}

	fetchAirports(airports) {
		this.airports = airports;
	}

	listAirportsAddress() {
		const tmp = this.airports.map(airport => airport.fullAddress);
		return tmp;
	}

	createSearchFlight(origin, destination, departureDate, adults, maxvalue, plane_checkbox, returnDate) {
		this.origin = origin;
		this.destination = destination;
		this.departureDate = departureDate;
		this.adults = adults;
		this.maxvalue = maxvalue;
		this.plane_checkbox = plane_checkbox;
		this.returnDate = returnDate;
	}

	createSearchHoteis(destino, checkin, checkout, adults, preco) {
		this.destination = destino;
		this.chekInDate = checkin;
		this.checkoutDate = checkout;
		this.adults = adults;
		this.price = preco;
	}

	createSearchPackage(origin, destination, checkin, checkout, maxvalue) {
		this.origin = origin;
		this.destination = destination;
		this.departureDate = checkin;
		this.returnDate = checkout;
		this.maxvalue = maxvalue;
	}

	getOrigin() {
		return this.origin;
	}

	getDestination() {
		return this.destination;
	}

	getDeparture() {
		return this.departureDate;
	}

	getAdults() {
		return this.adults;
	}

	getCheckinDate() {
		return this.chekInDate;
	}

	getCheckOutDate() {
		return this.checkoutDate;
	}

	getPrice() {
		return this.price;
	}

	getMaxValue() {
		return this.maxvalue;
	}

	isReturnTicket() {
		return this.plane_checkbox;
	}

	getReturnDate() {
		return this.returnDate;
	}
} 