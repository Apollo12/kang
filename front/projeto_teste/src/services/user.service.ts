import { Injectable } from '@angular/core';
import { User } from '../models/user';

@Injectable()
export class UserService {
	private current_user: User;

	constructor() {

	}

	Login(user) {
		this.current_user = user;
	}

	getUser() {
		return this.current_user;
	}

	Logout() {
		this.current_user = null;
	}
}