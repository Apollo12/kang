import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Cities } from '../../models/cities';

@Injectable()
export class CitiesProvider {
    private API_URL_CITIES = 'https://django-back-dot-still-totality-278901.rj.r.appspot.com/'

    constructor(public http: HttpClient) {
        console.log('Hello CitiesProvider Provider');
    }

    getCities() {
        return this.http.get<Cities[]>(this.API_URL_CITIES + 'kang/cities/')
            .map(res =>
                res);
    }

}