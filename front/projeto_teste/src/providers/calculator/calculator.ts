import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CalculoRes } from '../../pages/calculator/calculator';

@Injectable()
export class CalculatorProvider {
    private kotlinAPIcalculadora = 'https://still-totality-278901.rj.r.appspot.com/calculadora/'

    constructor(public http: HttpClient) {
        console.log('Hello Calculo');
    }

    getSubtracao(valorA, valorB) {
        console.log('iniciando subtracao dentro do provider');
        console.log(valorA);
        console.log(valorB);
        console.log(this.kotlinAPIcalculadora + 'subtrair?valorA=' + valorA + '&valorB=' + valorB);
        return this.http.get<number>(this.kotlinAPIcalculadora + 'subtrair?valorA=' + valorA + '&valorB=' + valorB)
            .map(res =>
                res);
    }

    getAdicao(valorA, valorB) {
        return this.http.get<number>(this.kotlinAPIcalculadora + 'somar?valorA=' + valorA + '&valorB=' + valorB)
            .map(res =>
                res);
    }

    getDivisao(valorA, valorB) {
        return this.http.get<number>(this.kotlinAPIcalculadora + 'dividir?valorA=' + valorA + '&valorB=' + valorB)
            .map(res =>
                res);
    }

    getMultiplicacao(valorA, valorB) {
        return this.http.get<number>(this.kotlinAPIcalculadora + 'multiplicar?valorA=' + valorA + '&valorB=' + valorB)
            .map(res =>
                res);
    }

}