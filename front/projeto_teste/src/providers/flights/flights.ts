import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Product } from '../../models/product';

@Injectable()
export class FlightsProvider {
	//private djangoAPI = 'http://127.0.0.1:8000/kang'
  private djangoAPI = 'https://django-back-dot-still-totality-278901.rj.r.appspot.com/kang'

  constructor(public http: HttpClient) {
    console.log('Hello FlightsProvider Provider');
  }

   getFlight(origin, destination, departureDate, adults, maxvalue) {
    return  this.http.get<Product[]>(this.djangoAPI + '/flight_search/' + origin + '/' + destination + '/' + departureDate + '/' + adults + '/' + maxvalue)
    .map(res =>
       res);
  }

   getFlightReturn(origin, destination, departureDate, adults, maxvalue, returnDate) {
    return  this.http.get<Product[]>(this.djangoAPI + '/flight_search/' + origin + '/' + destination + '/' + departureDate + '/' + adults + '/' + maxvalue + '/' + returnDate)
    .map(res =>
       res);
  }
}
