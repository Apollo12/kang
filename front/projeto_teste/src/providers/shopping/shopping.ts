import { Injectable } from '@angular/core';
import { Shopping } from '../../models/shopping';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { retry, catchError } from 'rxjs/operators';

@Injectable()
export class ShoppingProvider {
  //private djangoAPI = 'http://127.0.0.1:8000/kang'
  private djangoAPI = 'https://django-back-dot-still-totality-278901.rj.r.appspot.com/kang'

  constructor(public http: HttpClient) {
    console.log('Hello ShoppingProvider Provider');
  }

  // Headers
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  }

  getAll() {
    return  this.http.get<Shopping[]>(this.djangoAPI + '/shoppings/')
    .map(res =>
       res);
  }

  insert(shopping: Shopping) {
    return this.http.post<Shopping>(this.djangoAPI + '/shoppings/', JSON.stringify(shopping), this.httpOptions).
    pipe(
        retry(2),
        catchError(this.handleError));

  }


  handleError(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Erro ocorreu no lado do client
      errorMessage = error.error.message;
    } else {
      // Erro ocorreu no lado do servidor
      errorMessage = `Código do erro: ${error.status}, ` + `menssagem: ${error.message}`;
    }
    console.log(errorMessage);
    return errorMessage;
  };
}