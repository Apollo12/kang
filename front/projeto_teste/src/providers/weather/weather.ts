import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { WeatherResponse } from '../../models/weather';


@Injectable()
export class WeatherProvider {
    private API_URL_CLIMA = 'https://api.openweathermap.org/data/2.5/weather?&appid=89bc2153244c22f2c8e802a9a4e411e4&units=metric&lang=pt_br'

    constructor(public http: HttpClient) {
        console.log('Hello WeatherProvider Provider');
    }


    getWeather(lat, log) {
        console.log('getWeather');
        return this.http.get<WeatherResponse>(this.API_URL_CLIMA + '&lat=' + lat + '&lon=' + log)
            .map(res =>
                res);
    }

}