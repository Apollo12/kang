import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CotacaoRes } from '../../pages/exchange/exchange';

@Injectable()
export class CotacaoProvider {
	private kotlinAPI = 'https://still-totality-278901.rj.r.appspot.com/moeda/consultar?'

	constructor(public http: HttpClient) {
		console.log('Hello Cotacao');
	}

	getCotacao(moeda_local, moeda_pretendida, valor_cotacao) {
		return this.http.get<CotacaoRes>(this.kotlinAPI + 'moeda_local=' + moeda_local + '&moeda_pretendida=' + moeda_pretendida + '&valor_cotacao=' + valor_cotacao)
			.map(res =>
				res);
	}

	//   https://still-totality-278901.rj.r.appspot.com/moeda/consultar?moeda_local=USD-BRL&moeda_pretendida=EUR-BRL&valor_cotacao=5

	getTeste() {
		return new Promise((resolve, reject) => {
			let url = this.kotlinAPI + '/';
			this.http.get(url)
				.subscribe((result: any) => {
					resolve(result.json());
				},
					(error) => {
						reject(error.json());
					});
		});
	}

}
