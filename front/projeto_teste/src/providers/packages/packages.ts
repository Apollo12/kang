import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Package } from '../../models/package';

@Injectable()
export class PackagesProvider {
	//private djangoAPI = 'http://127.0.0.1:8000/kang'
  private djangoAPI = 'https://django-back-dot-still-totality-278901.rj.r.appspot.com/kang'

  constructor(public http: HttpClient) {
    console.log('Hello PackagesProvider Provider');
  }

   getPackages(origin, destination, departureDate, maxvalue, returnDate) {
    console.log(this.djangoAPI + '/package_search/' + origin + '/' + destination + '/' + departureDate + '/' + maxvalue + '/' + returnDate)
    return  this.http.get<Package[]>(this.djangoAPI + '/package_search/' + origin + '/' + destination + '/' + departureDate + '/' + maxvalue + '/' + returnDate)
    .map(res =>
       res);
  }
}