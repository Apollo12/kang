import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Product } from '../../models/product';


@Injectable()
export class hoteisProvider {
  private API_URL = 'https://still-totality-278901.rj.r.appspot.com/hotel/consultar?'
 
  constructor(public http: HttpClient) { console.log('Hello HoteisProvider Provider');}
 teste: HttpClient;
//https://still-totality-278901.rj.r.appspot.com/hotel/consultar?cidade=PAR&checkin=2020-07-01&checkout=2020-07-02&adulto=1&preco=160

  getHoteis(destino, checkin, checkout, adults, preco) {
    console.log('INICIANDO BUSCA PARA API: ');
    console.log(this.API_URL + 'cidade=' + destino + '&checkin=' + checkin + '&checkout=' + checkout + '&adulto=' + adults + '&preco=' + preco);
    return  this.http.get<Product[]>(this.API_URL + 'cidade=' + destino + '&checkin=' + checkin + '&checkout=' + checkout + '&adulto=' + adults + '&preco=' + preco)
    .map(res =>
       res);
  }

  getTeste(){
  	return new Promise((resolve, reject) => {
  	      let url = this.API_URL + '/';
  	      this.http.get(url)
  	        .subscribe((result: any) => {
  	          resolve(result.json());
  	        },
  	        (error) => {
  	          reject(error.json());
  	        });
  	    });
  }

 
}