import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Airport } from '../../models/airport';

@Injectable()
export class AirportsProvider {
	//private djangoAPI = 'http://127.0.0.1:8000/kang'
  private djangoAPI = 'https://django-back-dot-still-totality-278901.rj.r.appspot.com/kang'

  constructor(public http: HttpClient) {
    console.log('Hello AirportsProvider Provider');
  }

  getAll() {
    return  this.http.get<Airport[]>(this.djangoAPI + '/airports/');
  }

 /* async getAll() {
    const promise = new Promise((resolve, reject) => {
      const apiURL = (this.djangoAPI + 'airports/');
      this.http
        .get<Airport[]>(apiURL)
        .toPromise()
        .then((res: any) => {
          // Success
          this.data = res.map((res: any) => {
            return new Post(
              res.userId,
              res.id,
              res.title,
              res.body
            );
          });
          resolve();
        },
          err => {
            // Error
            reject(err);
          }
        );
    });
    return promise;
  } */

}