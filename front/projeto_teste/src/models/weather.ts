export class Weather {
    id: number;
    main: string;
    description: string;
    icon: string;
}

export class Weather_Main {
    temp: number;
    pressure: number;
    humidity: number;
    temp_min: number;
    temp_max: number;
    sea_level: number;
    grnd_level: number;
}

export class Weather_Wind {
    speed: number;
    deg: number;
}

export class WeatherResponse {
    weather: Weather[];
    main: Weather_Main;
    wind: Weather_Wind;
}

