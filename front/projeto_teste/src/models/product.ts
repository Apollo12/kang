export class Product {
	id_product : number;
	origin : string;
	nameProduct : string;
	checkin : Date;
	checkout : Date;
	price : number;
	amount : number;
	type_product : string;
}