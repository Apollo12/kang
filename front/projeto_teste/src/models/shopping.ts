export class Shopping {
	date_shopping: Date;
	origin: string;
	nameProduct: string;
	checkin: Date;
	checkout: Date;
	price: number;
	amount: number;
	type_product: string;
	client: number;


	constructor(date_shopping, origin, nameProduct, checkin, checkout, price, amount, type_product, client) {
		this.date_shopping = date_shopping;
		this.origin = origin;
		this.nameProduct = nameProduct;
		this.checkin = checkin;
		this.checkout = checkout;
		this.price = price;
		this.amount = amount;
		this.type_product = type_product;
		this.client = client;
	} 
}