import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, AlertController } from 'ionic-angular';
//import { BehaviorSubject } from 'rxjs';

import { Shopping } from '../../models/shopping';
import { Product } from '../../models/product';

import { CartService } from '../../services/cart.service';
import { UserService } from '../../services/user.service';

import { ShoppingProvider } from '../../providers/shopping/shopping';

@IonicPage()
@Component({
    selector: 'page-cart',
    templateUrl: 'cart.html'
})
export class CartPage implements OnInit {
  cart = [];
  shoppingList: Shopping[];

  constructor(public navCtrl: NavController, private cartService : CartService, private alertCtrl: AlertController,
                 private userService: UserService,  private shoppingProvider: ShoppingProvider) {
    }

  ngOnInit() {
    this.cart = this.cartService.getCart();
  }

  ionViewDidEnter() {
    this.shoppingList = []
  }

  decreaseCartItem(product) {
    this.cartService.decreaseProduct(product);
  }
  
  increaseCartItem(product) {
    this.cartService.addOneProduct(product);
  }
  
  removeCartItem(product) {
    this.cartService.removeProduct(product);
  }
  
  getTotal() {
    return this.cart.reduce((i, j) => i + j.price * j.amount, 0);
  }

  checkout() {
    // Efetua compra
  
    let alert = this.alertCtrl.create({
      title: 'Obrigado por comprar conosco!',
      message: 'Enviaremos um e-mail com as informações da sua compra',
      buttons: ['OK']
    });
    alert.present();
    this.createShoppingList();
    this.sendShoppingList();
    this.cartService.clearCart();
  }

  createShoppingList() {
    let currentDate = new Date();
    for (var product of this.cart) {
      let s = new Shopping(currentDate, product.origin, product.nameProduct,
                                  product.checkin, product.checkout, product.price, product.amount,
                                     product.type_product, this.userService.getUser().id);
      this.shoppingList.push(s);
    } 
  } 

  sendShoppingList() {
    for (var shopping of this.shoppingList) {
      this.shoppingProvider.insert(shopping).subscribe();
    }
  } 


}