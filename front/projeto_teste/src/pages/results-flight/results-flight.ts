import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, ToastController, LoadingController } from 'ionic-angular';

import { CartService } from '../../services/cart.service';
import { BehaviorSubject } from 'rxjs';

import { FlightsProvider} from '../../providers/flights/flights';
import { Product } from '../../models/product';
import { SearchService } from '../../services/search.service';

//nova versão

@IonicPage()
@Component({
  selector: 'page-results-flight',
  templateUrl: 'results-flight.html',
})
export class ResultsFlightPage implements OnInit {
    private voos: Product[];
    private cart = [];
    private cartItemCount: BehaviorSubject<number>;
    private respostaVazia = 2;

    constructor(
        public navCtrl: NavController, private toastCtrl : ToastController, public loadingController: LoadingController,
        private cartService : CartService, private flightsProvider : FlightsProvider, private searchService: SearchService) {
    }

    ngOnInit() {
       this.cart = this.cartService.getCart();
       this.cartItemCount = this.cartService.getCartItemCount();
       this.voos = [];
        this.searchFlight(this.searchService.getOrigin(), this.searchService.getDestination(),
    					this.searchService.getDeparture(), this.searchService.getAdults(), this.searchService.getMaxValue(),
              this.searchService.isReturnTicket(), this.searchService.getReturnDate());
    }

    ionViewDidEnter() {
        

  }

    addToCart(product) {
        this.cartService.addProduct(product);
        let toast = this.toastCtrl.create({
          message: "O voô foi adicionado ao carrinho",
          duration: 3000,
          position: 'bottom'
        });
        toast.present();
    }

    openCart() {
    this.navCtrl.push('CartPage');
    }


    async searchFlight(origin, destination, departureDate, adults, maxValue, plane_checkbox, returnDate) {
      const loading = this.loadingController.create({
        cssClass: 'my-custom-class',
        content: 'Aguarde...'
    });
    await loading.present();
      if(plane_checkbox) {
        this.flightsProvider.getFlightReturn(origin, destination, departureDate, adults, maxValue, returnDate).
          subscribe((res: Product[]) =>{
                 this.voos = res;
                 console.log(this.voos);
                 loading.dismiss();
                 if (this.voos.length == 0){
                    console.log("resposta veio vazia");
                    this.respostaVazia = 1;
                }
                else{
                    this.respostaVazia = 0;
                }    
          });
      } else {
        this.flightsProvider.getFlight(origin, destination, departureDate, adults, maxValue).
          subscribe((res: Product[]) =>{
                 this.voos = res;
                 console.log(this.voos);
                 loading.dismiss();
                 if (this.voos.length == 0){
                    console.log("resposta veio vazia");
                    this.respostaVazia = 1;
                }
                else{
                    this.respostaVazia = 0;
                }    
          });
        }
        
    }


}
