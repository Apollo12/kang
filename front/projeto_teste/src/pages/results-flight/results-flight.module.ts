import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ResultsFlightPage } from './results-flight';

@NgModule({
  declarations: [
    ResultsFlightPage,
  ],
  imports: [
    IonicPageModule.forChild(ResultsFlightPage),
  ],
})
export class ResultsFlightPageModule {}
