import { Component } from '@angular/core';
import { IonicPage } from 'ionic-angular';

/**
 * The Settings page is a simple form that syncs with a Settings provider
 * to enable the user to customize settings for the app.
 *
 */
@IonicPage()
@Component({
    selector: 'page-register',
    templateUrl: 'register.html'
})
export class RegisterPage {

}