import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, ToastController, LoadingController } from 'ionic-angular';

import { CartService } from '../../services/cart.service';
import { BehaviorSubject } from 'rxjs';

import { PackagesProvider } from '../../providers/packages/packages';
import { FlightsProvider } from '../../providers/flights/flights';
import { Product } from '../../models/product';
import { Package } from '../../models/package';
import { SearchService } from '../../services/search.service';


@IonicPage()
@Component({
  selector: 'page-results-package',
  templateUrl: 'list-master.html',
})
export class ListMasterPage implements OnInit {

    private packages: Package[];
    private flights: Product[];
    private cart = [];
    private cartItemCount: BehaviorSubject<number>;
    private cities = ['PAR']
//    private cities = ['PAR','BER', 'BKK', 'LJU', 'LUX']
    private city: string;
    //private cities = ['AGT', 'ANR', 'AMS', 'BER', 'BFV', 'BIO', 'BLQ', 'BLE']

    constructor(
        public navCtrl: NavController, private toastCtrl : ToastController,
        private cartService : CartService, private packagesProvider : PackagesProvider, private searchService: SearchService,
                    private flightsProvider : FlightsProvider, public loadingController: LoadingController) {
    }

    ngOnInit() {
       this.cart = this.cartService.getCart();
       this.cartItemCount = this.cartService.getCartItemCount();
       this.packages = [];
       this.city = this.pickCity();
       this.searchPackages();

    }

    ionViewWillEnter() {

  }

    addToCart(p) {
        this.cartService.addProduct(p.flight);
        this.cartService.addProduct(p.hotel);
        let toast = this.toastCtrl.create({
          message: "O pacote foi adicionado ao carrinho",
          duration: 3000,
          position: 'bottom'
        });
        toast.present();
    }

    openCart() {
    this.navCtrl.push('CartPage');
    }

    // async searchPackages() {
    //   let origin = "SAO"
    //   let destination = this.city;
    //   let checkin = '2021-01-02';
    //   let maxvalue = 4000;
    //   let checkout = '2021-01-04'

    //   const loading = this.loadingController.create({
    //     cssClass: 'my-custom-class',
    //     content: 'Aguarde...'
    // });
    // await loading.present();
    //   this.flightsProvider.getFlightReturn(origin, destination, checkin, 1, maxvalue, checkout).
    //     subscribe((res: Product[]) =>{
    //            this.flights = res;
    //            loading.dismiss();
    //         });
    // }

      async searchPackages() {
      let origin = "SAO"
      let destination = this.city;
      let checkin = '2021-01-02';
      let maxvalue = 4000;
      let checkout = '2021-01-04'

      const loading = this.loadingController.create({
        cssClass: 'my-custom-class',
        content: 'Aguarde...'
    });
    await loading.present();
      this.packagesProvider.getPackages(origin, destination, checkin, maxvalue, checkout).
        subscribe((res: Package[]) =>{
               this.packages = res;
               loading.dismiss();
            });
    }

    pickCity() {
      return this.cities[Math.floor(Math.random() * this.cities.length)];
    }



}
