import { Component, OnInit} from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { IonicPage, NavController, ToastController, LoadingController } from 'ionic-angular';
import { FormsModule } from '@angular/forms';

import { AirportsProvider } from '../../providers/airports/airports';
import { UserProvider } from '../../providers/user/user';
import { MainPage } from '../';
import { User } from '../../models/user';
import { Airport } from '../../models/airport';
import { UserService } from '../../services/user.service';
import { SearchService } from '../../services/search.service';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})

export class LoginPage implements OnInit {
  // The account fields for the login form.
  // If you're using the username field with or without email, make
  // sure to add it to the type
  user_email: string;
  user_password: string;
  users: User[];
  airports: Airport[];
  imports: [
    FormsModule
  ];
  constructor(public navCtrl: NavController, public toastCtrl: ToastController, public loadingController: LoadingController, 
              public translateService: TranslateService, private usersProvider: UserProvider, private userService: UserService,
              private searchService: SearchService, private airportsProvider: AirportsProvider) {
               
  }
  ngOnInit() {
    this.users = []
    this.fetchUsers();

  }
  ionViewDidEnter() {
  }

  async fetchUsers() {
    const loading = this.loadingController.create({
      cssClass: 'my-custom-class',
      content: 'Aguarde...'
  });
  await loading.present();
    this.usersProvider.getAll().
      subscribe((res: User[]) =>{
                 this.users = res;
                 loading.dismiss();
                });
  }

  doLogin() {
    let not_found = true;
    if(this.user_email == null || this.user_email.length == 0){
      let toast = this.toastCtrl.create({
        message: "Erro o campo EMAIL esta vazio, por favor preencha o campo antes de continuar",
        duration: 3000,
        position: 'top'
      });
      toast.present();
    }else if(!/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/.test(this.user_email)){
      let toast = this.toastCtrl.create({
        message: "EMAIL invalido, por favor preencha com um Email valido",
        duration: 3000,
        position: 'top'
      });
      toast.present();
    }else if(this.user_password == null || this.user_password.length == 0){
      let toast = this.toastCtrl.create({
        message: "Erro o campo SENHA esta vazio, por favor preencha o campo antes de continuar",
        duration: 3000,
        position: 'top'
      });
      toast.present();
    }
    else{
      
        for (var user_object of this.users) {
          if((user_object.email.toLowerCase()) === (this.user_email.toLowerCase())) {
            not_found = false;
    
            if(user_object.password === this.user_password) {
              this.userService.Login(user_object);
              this.navCtrl.push(MainPage);
              
            } else {
              let toast = this.toastCtrl.create({
                message: "Não foi possível logar. Verifique sua senha!",
                duration: 3000,
                position: 'top'
              });
              toast.present();
              break;
            }
          }
        }
        if (not_found){
          let toast = this.toastCtrl.create({
            message: "E-mail não encontrado. Efetue o cadastro na tela principal",
            duration: 3000,
            position: 'top'
          });
          toast.present();
        }
      
      
    }
    
  }
}
