import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';

import { CalculatorPage } from './calculator';

@NgModule({
    declarations: [
        CalculatorPage,
    ],
    imports: [
        IonicPageModule.forChild(CalculatorPage),
        TranslateModule.forChild()
    ],
    exports: [
        CalculatorPage
    ]
})
export class CalculatorPageModule { }