import { IonicPage, NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';
import { Component } from '@angular/core';
import { CalculatorProvider } from '../../providers/calculator/calculator';

/**
 * The Settings page is a simple form that syncs with a Settings provider
 * to enable the user to customize settings for the app.
 *
 */
@IonicPage()
@Component({
    selector: 'page-calculator',
    templateUrl: 'calculator.html'
})
export class CalculatorPage {
    valorA: number;
    valorB: number;
    calculo: number;
    operacao_escolhida: String;

    constructor(public navCtrl: NavController, public navParams: NavParams, public calculatorProvider: CalculatorProvider, public loadingController: LoadingController, private toastCtrl: ToastController) { }

    shownGroup = null;

    toggleGroup(group) {
        if (this.isGroupShown(group)) {
            this.shownGroup = null;
        } else {
            this.shownGroup = group;
        }
    };

    isGroupShown(group) {
        return this.shownGroup === group;
    };

    isOperacoesFromAvailable = true;
    operacoes = [];

    initializeOperacoes() {
        this.operacoes = ['Adição',
            'Subtração',
            'Multiplicação',
            'Divisão']
    }

    getOperacoes(ev) {
        // Reset items back to all of the items
        this.initializeOperacoes();

        // set val to the value of the searchbar
        const val = ev.target.value;

        // if the value is an empty string don't filter the items
        if (val && val.trim() !== '') {
            this.isOperacoesFromAvailable = true;
            this.operacoes = this.operacoes.filter((item) => {
                return (item.toLowerCase().indexOf(val.toLowerCase()) > -1);
            })
        } else {
            this.isOperacoesFromAvailable = false;
        }
    }


    radioGroupChange(ev) {
        console.log('radioGroupChange');
        this.operacao_escolhida = this.operacao_escolhida;
    }

    async search_calculator() {
        if (this.valorA == null) {
            let toast = this.toastCtrl.create({
              message: "Erro o campo VALOR A esta vazio, por favor preencha o campo antes de continuar",
              duration: 3000,
              position: 'top'
            });
            toast.present();
          } else if (this.valorB == null) {
            let toast = this.toastCtrl.create({
              message: "Erro o campo VALOR B esta vazio, por favor preencha o campo antes de continuar",
              duration: 3000,
              position: 'top'
            });
            toast.present();
          } else if (isNaN(this.valorA) || isNaN(this.valorB)) {
              console.log("ISNAN")
              let toast = this.toastCtrl.create({
                  message: "Erro, valores informados não são número válidos",
                  duration: 3000,
                  position: 'top'
              });
              toast.present();
          } else if (this.operacao_escolhida == null) {
              let toast = this.toastCtrl.create({
                  message: "Erro, nenhuma operação escolhida!",
                  duration: 3000,
                  position: 'top'
              });
              toast.present();
          }
          else{
            const loading = this.loadingController.create({
                cssClass: 'my-custom-class',
                content: 'Aguarde...'
            });
            console.log('iniciando calculo');
            console.log(this.valorA);
            console.log(this.valorB);
            await loading.present();
            if (this.operacao_escolhida == 'subtracao') {
                console.log('iniciando subtracao');
                this.calculatorProvider.getSubtracao(this.valorA, this.valorB).
                    subscribe((res: number) =>{
                        this.calculo = res;
                        loading.dismiss();
                    });
            }
            else if (this.operacao_escolhida == 'adicao') {
                console.log('iniciando adicao');
                this.calculatorProvider.getAdicao(this.valorA, this.valorB).
                    subscribe((res: number) =>{
                        this.calculo = res;
                        loading.dismiss();
                    });
                console.log('adicao=' + this.calculo);
            }
            else if (this.operacao_escolhida == 'divisao') {
                console.log('iniciando divisao');
                this.calculatorProvider.getDivisao(this.valorA, this.valorB).
                    subscribe((res: number) =>{
                        this.calculo = res;
                        loading.dismiss();
                    });
                console.log('divisao=' + this.calculo);
            }
            else if (this.operacao_escolhida == 'multiplicacao') {
                console.log('iniciando multiplicacao');
                this.calculatorProvider.getMultiplicacao(this.valorA, this.valorB).
                    subscribe((res: number) =>{
                        this.calculo = res;
                        loading.dismiss();}
                        );
                console.log('multiplicacao=' + this.calculo);
            }
    }
    }

    isCalculator() {
        if (this.calculo == null) {
            return false;
        }
        return true;
    }

    erase_calculator() {
        this.navCtrl.push('CalculatorPage');
    }
}

export class CalculoRes {
    calculo: number;
}