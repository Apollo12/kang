import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';

import { ExchangePage } from './exchange';

@NgModule({
    declarations: [
        ExchangePage,
    ],
    imports: [
        IonicPageModule.forChild(ExchangePage),
        TranslateModule.forChild()
    ],
    exports: [
        ExchangePage
    ]
})
export class ExchangePageModule { }
