import { IonicPage, NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';
import { Component } from '@angular/core';
import { CotacaoProvider} from '../../providers/cotacao/cotacao';


@IonicPage()
@Component({
    selector: 'page-exchange',
    templateUrl: 'exchange.html'
})
export class ExchangePage {
private cotacao: CotacaoRes;
moeda_local: String; 
moeda_pretendida: String; 
valor_cotacao: String;
moeda_local_cv: String;
moeda_pretendida_cv: String;

    constructor(public navCtrl: NavController, public navParams: NavParams, public cotacaoProvider: CotacaoProvider,  public loadingController: LoadingController, private toastCtrl: ToastController) { 

        console.log('Hello Exchange');
    }

    shownGroup = null;

    toggleGroup(group) {
        if (this.isGroupShown(group)) {
            this.shownGroup = null;
        } else {
            this.shownGroup = group;
        }
    };

    isGroupShown(group) {
        return this.shownGroup === group;
    };

    isCoinFromAvailable = true;
    moedas = [];

    initializeItemsOrigem() {
        this.moedas = ['USD (Dólar Comercial)', 
            'CAD (Dólar Canadense)',
            'AUD (Dólar Australiano)',
            'EUR (Euro)',
            'GBP (Libra Esterlina)',
            'ARS (Peso Argentino)',
            'JPY (Iene Japonês)',
            'CHF (Franco Suíço)',
            'CNY (Yuan Chinês)']
    }

    getCoinsOrigemPsg(ev) {
        // Reset items back to all of the items
        this.initializeItemsOrigem();

        // set val to the value of the searchbar
        const val = ev.target.value;

        // if the value is an empty string don't filter the items
        if (val && val.trim() !== '') {
            this.isCoinFromAvailable = true;
            this.moedas = this.moedas.filter((item) => {
                return (item.toLowerCase().indexOf(val.toLowerCase()) > -1);
            })
        } else {
            this.isCoinFromAvailable = false;
        }
    }

    moedasDestino = [];

    initializeItemsDestino() {
        this.moedasDestino = ['USD (Dólar Comercial)',
            'CAD (Dólar Canadense)',
            'AUD (Dólar Australiano)',
            'EUR (Euro)',
            'GBP (Libra Esterlina)',
            'ARS (Peso Argentino)',
            'JPY (Iene Japonês)',
            'CHF (Franco Suíço)',
            'CNY (Yuan Chinês)']
    }

    getCoinsDestinoPsg(ev) {
        // Reset items back to all of the items
        this.initializeItemsDestino();

        // set val to the value of the searchbar
        const val = ev.target.value;

        // if the value is an empty string don't filter the items
        if (val && val.trim() !== '') {
            this.isCoinFromAvailable = true;
            this.moedas = this.moedas.filter((item) => {
                return (item.toLowerCase().indexOf(val.toLowerCase()) > -1);
            })
        } else {
            this.isCoinFromAvailable = false;
        }
    }

    async search_cotacao() {
        if(this.valor_cotacao == null){
            let toast = this.toastCtrl.create({
                message: "Erro o campo VALOR esta vazio, por favor preencha o campo antes de continuar",
                duration: 3000,
                position: 'top'
            });
            toast.present();
        } else if (this.moeda_local == null) {
            let toast = this.toastCtrl.create({
              message: "Erro o campo MOEDA LOCAL esta vazio, por favor preencha o campo antes de continuar",
              duration: 3000,
              position: 'top'
            });
            toast.present();
        } else if (this.moeda_pretendida == null) {
            let toast = this.toastCtrl.create({
              message: "Erro o campo MOEDA PRETENDIDA esta vazio, por favor preencha o campo antes de continuar",
              duration: 3000,
              position: 'top'
            });
            toast.present();
        } else if (isNaN(parseInt(this.valor_cotacao.toString()))) {
            console.log("entrou regra do valor com string")
              let toast = this.toastCtrl.create({
              message: "Erro o campo VALOR contem letras, por favor preencha o campo com numeros",
              duration: 3000,
              position: 'top'
            });
            toast.present();

        } else if (parseInt(this.valor_cotacao.toString()) < 0){
            console.log("entrou regra do menor que 0")
              let toast = this.toastCtrl.create({
              message: "Erro o campo VALOR nao pode ser negativo",
              duration: 3000,
              position: 'top'
            });
            toast.present();
        } else{
            this.moeda_local_cv = this.converteMoeda(this.moeda_local);
            this.moeda_pretendida_cv = this.converteMoeda(this.moeda_pretendida);
            const loading = this.loadingController.create({
                cssClass: 'my-custom-class',
                content: 'Aguarde...'
            });
            await loading.present();
            this.cotacaoProvider.getCotacao(this.moeda_local_cv, this.moeda_pretendida_cv, this.valor_cotacao).
                    subscribe((res: CotacaoRes) =>{
                         this.cotacao = res;
                         console.log(this.cotacao);
                         loading.dismiss();
                        });
                
          }
    
    }
    
    converteMoeda(moeda_nome){
        if(moeda_nome == 'USD (Dólar Comercial)'){
            return 'USD-BRL';

        } else if (moeda_nome == 'CAD (Dólar Canadense)') {
            return 'CAD-BRL';
        
        } else if (moeda_nome == 'EUR (Euro)') {
            return 'EUR-BRL';

        } else if (moeda_nome == 'GBP (Libra Esterlina)') {
            return 'GBP-BRL';

        } else if (moeda_nome == 'ARS (Peso Argentino)') {
            return 'ARS-BRL';

        } else if (moeda_nome == 'JPY (Iene Japonês)') {
            return 'JPY-BRL';

        } else if (moeda_nome == 'CHF (Franco Suíço)') {
            return 'CHF-BRL';

        } else if (moeda_nome == 'CNY (Yuan Chinês)') {
            return 'CNY-BRL';
        }
        return '';
    }

    isCotacao (){
        if (this.cotacao == null) {
            return false;
        }

        return true;
    }
}

export class CotacaoRes {
valor: number;
}