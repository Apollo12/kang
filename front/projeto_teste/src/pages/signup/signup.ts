import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { IonicPage, NavController, ToastController, LoadingController } from 'ionic-angular';

import { MainPage } from '../';
import { WelcomePage } from '../welcome/welcome';
import { User } from '../../models/user';

import { UserProvider } from '../../providers/user/user';

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html'
})
export class SignupPage implements OnInit {
  // The account fields for the login form.
  // If you're using the username field with or without email, make
  // sure to add it to the type
  account: User;
  users: User[];


  constructor(public navCtrl: NavController, public loadingController: LoadingController, 
    public toastCtrl: ToastController,
    public translateService: TranslateService, private usersProvider: UserProvider) {

  }

  ngOnInit() {
    this.account = new User();
    this.users = [];
    this.fetchUsers();
  }

  ionViewDidEnter() {


  }

  async fetchUsers() {
    const loading = this.loadingController.create({
      cssClass: 'my-custom-class',
      content: 'Aguarde...'
  });
  await loading.present();
    this.usersProvider.getAll().
      subscribe((res: User[]) =>{
                 this.users = res;
                 loading.dismiss();
                });
  }


  doSignup() {
    if(this.account.name == null || this.account.name.length == 0){
      let toast = this.toastCtrl.create({
        message: "Erro o campo NOME esta vazio, por favor preencha o campo antes de continuar",
        duration: 3000,
        position: 'top'
      });
      toast.present();
    }else if(this.account.email == null || this.account.email.length == 0){
      let toast = this.toastCtrl.create({
        message: "Erro o campo email esta vazio, por favor preencha o campo antes de continuar",
        duration: 3000,
        position: 'top'
      });
      toast.present();
    }else  if(!/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/.test(this.account.email)){
      let toast = this.toastCtrl.create({
        message: "EMAIL invalido, por favor preencha com um Email valido",
        duration: 3000,
        position: 'top'
      });
      toast.present();
    }else if(this.account.password == null|| this.account.password.length == 0){
      let toast = this.toastCtrl.create({
        message: "Erro o campo SENHA esta vazio, por favor preencha o campo antes de continuar",
        duration: 3000,
        position: 'top'
      });
      toast.present();
    }else{
      let found = false;

      for (var user_object of this.users) {
        if((user_object.email.toLowerCase()) === (this.account.email.toLowerCase())) {
          found = true;
          break;
        }
      }
  
      if(found) {
          let toast = this.toastCtrl.create({
            message: "E-mail já cadastrado, por favor escolha outra opção",
            duration: 3000,
            position: 'top'
          });
          toast.present();
      } else {
        this.usersProvider.insert(this.account).
         subscribe();
         let toast = this.toastCtrl.create({
           message: "Usuário cadastrado com sucesso!",
           duration: 3000,
           position: 'top'
         });
         toast.present();
         this.navCtrl.push(WelcomePage);
      }
  
    }
    }
    



}
