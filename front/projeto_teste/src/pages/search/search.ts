
import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, LoadingController  } from 'ionic-angular';

import { Airport } from '../../models/airport';

import { FlightsProvider } from '../../providers/flights/flights'
import { SearchService } from '../../services/search.service';
import { AirportsProvider } from '../../providers/airports/airports';
import { c } from '@angular/core/src/render3';

@IonicPage()
@Component({
  selector: 'page-search',
  templateUrl: 'search.html'
})
export class SearchPage implements OnInit {
  shownGroup = null;
  airports: Airport[];
  todayDate: Date = new Date();
  plane_checkbox;

  item;
  //PASSAGEM
  origin: string;
  destination: string;
  departure: Date;
  returnDate: Date;
  PSGadults = 1;
  PSGValue = 5000;

  //HOTEL
  hotelOrigin: string;
  checkInDate: Date;
  checkOutDate: Date;
  hotelPrice = 5500;
  hotelAdults = 1;

  // pacotes
  pkgOrigin: string;
  pkgDestination: string;
  checkin: Date;
  checkout: Date;
  pkgValue = 10000;


  constructor(public navCtrl: NavController, public navParams: NavParams, public loadingController: LoadingController,
    private searchService: SearchService, private toastCtrl: ToastController, private airportsProvider: AirportsProvider) {
    console.log('Hello Search.ts');


  }


  async ngOnInit() {
    this.item = "passagem";
    const loading = this.loadingController.create({
      cssClass: 'my-custom-class',
      content: 'Aguarde...'
  });
  await loading.present();
    this.airportsProvider.getAll()
      .subscribe((res: Airport[]) => {this.airports = res;  loading.dismiss();});
    //this.limpaCampos();
  }

  ionViewWillEnter() {

  }

  ionViewDidEnter() {
  }

  limpaCampos(){
    //Teste zerar os campos ao entrar na pagina
    this.origin = null;
    this.destination = null;
    this.departure = null;
    this.PSGadults = 1;
    this.PSGValue = 5000;

    this.hotelOrigin = null;
    this.checkInDate = null;
    this.checkOutDate = null;
    this.hotelPrice = 5500;
    this.hotelAdults = 1;
  }

  //PASSAGEM
  psgOriginItems: any = [];

  getItemsOrigemPsg(ev) {

    let val = ev.target.value;
    if (!val || !val.trim()) {
      this.psgOriginItems = [];
      return;
    }
    this.psgOriginItems = this.airports.filter(airport => airport.fullAddress.toUpperCase().includes(val.toUpperCase()));

  }

  // groupsVisible: boolean[] = [ true, true ];

  toggleGroup(group) {
    if (this.isGroupShown(group)) {
      this.shownGroup = null;
    } else {
      this.shownGroup = group;
    }
  };

  isGroupShown(group) {
    return this.shownGroup === group;
  };


  PsgdestinyItems: any = [];

  getItemsDestinoPsg(ev) {
    let val = ev.target.value;
    if (!val || !val.trim()) {
      this.PsgdestinyItems = [];
      return;
    }
    this.PsgdestinyItems = this.airports.filter(airport => airport.fullAddress.toUpperCase().includes(val.toUpperCase()) && airport.isBookable);
  }

  /*PsgdestinySoIdaItems: any = [];

  getItemsDestinoSoIdaPsg(ev) {
    let val = ev.target.value;
    if (!val || !val.trim()) {
      this.PsgdestinySoIdaItems = [];
      return;
    }
    this.PsgdestinySoIdaItems = this.items.query({
      name: val
    });
  } */

  //HOTEL
  htlDestinyItems: any = [];

  getItemsDestinoHtl(ev) {
    let val = ev.target.value;
    if (!val || !val.trim()) {
      this.htlDestinyItems = [];
      return;
    }
    this.htlDestinyItems = this.airports.filter(airport => airport.fullAddress.toUpperCase().includes(val.toUpperCase()) && airport.isBookable);
  }

  //PACOTE

  pctOriginItems: any = [];

  getItemsOrigemPct(ev) {
    let val = ev.target.value;
    if (!val || !val.trim()) {
      this.pctOriginItems = [];
      return;
    }
    this.pctOriginItems = this.airports.filter(airport => airport.fullAddress.toUpperCase().includes(val.toUpperCase()));

  }

  pctDestinyItems: any = [];

  getItemsDestinoPct(ev) {
    let val = ev.target.value;
    if (!val || !val.trim()) {
      this.pctDestinyItems = [];
      return;
    }
    this.pctDestinyItems = this.airports.filter(airport => airport.fullAddress.toUpperCase().includes(val.toUpperCase()) && airport.isBookable);
  }

  buttonColor: string = "primary";

  searchHoteis() {
    let checkinValidacao: Date = new Date(this.checkInDate);
    let checkoutValidacao: Date = new Date(this.checkOutDate);
    console.log("hotel");


    if (this.hotelOrigin == null) {
      let toast = this.toastCtrl.create({
        message: "Erro o campo DESTINO esta vazio, por favor preencha o campo antes de continuar",
        duration: 3000,
        position: 'top'
      });
      toast.present();
    } else if (this.checkInDate == null) {
      let toast = this.toastCtrl.create({
        message: "Erro o campo DATA DE ENTRADA esta vazio, por favor preencha o campo antes de continuar",
        duration: 3000,
        position: 'top'
      });
      toast.present();
    } else if (this.checkOutDate == null) {
      let toast = this.toastCtrl.create({
        message: "Erro o campo DATA DE SAIDA esta vazio, por favor preencha o campo antes de continuar",
        duration: 3000,
        position: 'top'
      });
      toast.present();
    } else if (this.hotelAdults == null) {
      let toast = this.toastCtrl.create({
        message: "Erro o campo PESSOAS esta vazio, por favor preencha o campo antes de continuar",
        duration: 3000,
        position: 'top'
      });
      toast.present();
    } else if (this.hotelPrice == null) {
      let toast = this.toastCtrl.create({
        message: "Erro o campo DIARIA esta vazio, por favor preencha o campo antes de continuar",
        duration: 3000,
        position: 'top'
      });
      toast.present();
    }
    else {
      let dataMAXIMA = new Date(this.todayDate.getFullYear(), (this.todayDate.getMonth() + 4), 0);
      if (checkinValidacao < this.todayDate) {
        let toast = this.toastCtrl.create({
          message: "Erro o campo CHECKIN esta com data menor que a suportada",
          duration: 3000,
          position: 'top'
        });
        toast.present();
      } else if (checkinValidacao > dataMAXIMA) {
        let toast = this.toastCtrl.create({
          message: "Erro o campo CHECKIN esta com data maior que a suportada",
          duration: 3000,
          position: 'top'
        });
        toast.present();
      } else if (checkoutValidacao < checkinValidacao) {
        let toast = this.toastCtrl.create({
          message: "Erro o campo CHECKOUT esta com data menor que a suportada",
          duration: 3000,
          position: 'top'
        });
        toast.present();
      } else if (checkoutValidacao > dataMAXIMA) {
        let toast = this.toastCtrl.create({
          message: "Erro o campo CHECKOUT esta com data maior que a suportada",
          duration: 3000,
          position: 'top'
        });
        toast.present();
      }
      else {
        console.log('Iniciando Busca Hoteis');
        console.log(this.hotelOrigin);
        let htlOrigin = this.convertLocation(this.hotelOrigin);
        console.log(htlOrigin);
        console.log(this.checkInDate);
        console.log(this.checkOutDate);
        console.log(this.hotelAdults);
        console.log(this.hotelPrice);
        this.searchService.createSearchHoteis(
          htlOrigin, this.checkInDate, this.checkOutDate, this.hotelAdults, this.hotelPrice);
        this.navCtrl.push('ResultsHoteisPage');
      }

    }

  }

  searchFlights() {
    let departureValidacao: Date = new Date(this.departure);
    console.log("passagens");

    if (this.origin == null) {
      let toast = this.toastCtrl.create({
        message: "Erro o campo ORIGEM esta vazio, por favor preencha o campo antes de continuar",
        duration: 3000,
        position: 'top'
      });
      toast.present();
    } else if (this.destination == null) {
      let toast = this.toastCtrl.create({
        message: "Erro o campo DESTINO esta vazio, por favor preencha o campo antes de continuar",
        duration: 3000,
        position: 'top'
      });
      toast.present();
    } else if (departureValidacao < this.todayDate) {
        let toast = this.toastCtrl.create({
          message: "Erro o campo DATA DE IDA esta com data menor que a suportada",
          duration: 3000,
          position: 'top'
        });
        toast.present();
    } else if (this.PSGadults == null) {
      let toast = this.toastCtrl.create({
        message: "Erro o campo ADULTO esta vazio, por favor preencha o campo antes de continuar",
        duration: 3000,
        position: 'top'
      });
      toast.present();
    } else if (this.departure == null) {
      let toast = this.toastCtrl.create({
        message: "Erro o campo DATA DE IDA esta vazio, por favor preencha o campo antes de continuar",
        duration: 3000,
        position: 'top'
      });
      toast.present();
    } else if (this.PSGValue == null) {
      let toast = this.toastCtrl.create({
        message: "Erro o campo VALOR esta vazio, por favor preencha o campo antes de continuar",
        duration: 3000,
        position: 'top'
      });
      toast.present();
    }
    else {
      let origin = this.convertLocation(this.origin);
      let destination = this.convertLocation(this.destination);
      let returnDateValidacao: Date = new Date(this.returnDate);
      if (this.plane_checkbox == true) {
        if(this.returnDate == null){
          let toast = this.toastCtrl.create({
            message: "Erro o campo DATA DE VOLTA esta vazio, por favor preencha o campo antes de continuar",
            duration: 3000,
            position: 'top'
          });
          toast.present();
        } else if (returnDateValidacao < departureValidacao) {
        let toast = this.toastCtrl.create({
          message: "Erro o campo DATA DE VOLTA esta com data menor que a suportada",
          duration: 3000,
          position: 'top'
        });
        toast.present();
        }

      }
      this.searchService.createSearchFlight(
        origin, destination, this.departure, this.PSGadults, this.PSGValue, this.plane_checkbox, this.returnDate);
      this.navCtrl.push('ResultsFlightPage');
    }
  }

  searchPackages() {
    let checkin_p_validacao: Date = new Date(this.checkin);
    let checkout_p_validacao: Date = new Date(this.checkout);
    console.log("pacotes");
    if (this.pkgOrigin == null) {
      let toast = this.toastCtrl.create({
        message: "Erro o campo ORIGEM esta vazio, por favor preencha o campo antes de continuar",
        duration: 3000,
        position: 'top'
      });
      toast.present();
    } else if (this.pkgDestination == null) {
      let toast = this.toastCtrl.create({
        message: "Erro o campo DESTINO esta vazio, por favor preencha o campo antes de continuar",
        duration: 3000,
        position: 'top'
      });
      toast.present();
    } else if (this.checkin == null) {
      let toast = this.toastCtrl.create({
        message: "Erro o campo DATA DE IDA esta vazio, por favor preencha o campo antes de continuar",
        duration: 3000,
        position: 'top'
      });
      toast.present();
    } else if (checkin_p_validacao < this.todayDate) {
      let toast = this.toastCtrl.create({
        message: "Erro o campo CHECKIN esta com data menor que a suportada",
        duration: 3000,
        position: 'top'
      });
      toast.present();
    } else if (this.checkout == null) {
      let toast = this.toastCtrl.create({
        message: "Erro o campo DATA DE VOLTA esta vazio, por favor preencha o campo antes de continuar",
        duration: 3000,
        position: 'top'
      });
      toast.present();
    } else if (checkout_p_validacao < checkin_p_validacao) {
      let toast = this.toastCtrl.create({
        message: "Erro o campo CHECKOUT esta com data menor que a suportada",
        duration: 3000,
        position: 'top'
      });
      toast.present();
    } else if (this.pkgValue == null) {
      let toast = this.toastCtrl.create({
        message: "Erro o campo VALOR esta vazio, por favor preencha o campo antes de continuar",
        duration: 3000,
        position: 'top'
      });
      toast.present();
    }
    else {
      let origin = this.convertLocation(this.pkgOrigin);
      let destination = this.convertLocation(this.pkgDestination);
      this.searchService.createSearchPackage(origin, destination, this.checkin, this.checkout, this.pkgValue);
      this.navCtrl.push('ResultsPackagePage');
    }
  }

  convertLocation(location) {
    let airport = this.airports.find(airport => airport.fullAddress === location);
    return airport.iataCode;
  }
}
