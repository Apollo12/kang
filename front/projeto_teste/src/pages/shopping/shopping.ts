import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';

import { Shopping } from '../../models/shopping';
import { ShoppingProvider } from '../../providers/shopping/shopping';

import { UserService } from '../../services/user.service';

/**
 * The Settings page is a simple form that syncs with a Settings provider
 * to enable the user to customize settings for the app.
 *
 */
@IonicPage()
@Component({
    selector: 'page-shopping',
    templateUrl: 'shopping.html'
})
export class ShoppingPage {
    shoppingList: Shopping[];

    constructor(public navCtrl: NavController, private shoppingProvider: ShoppingProvider, private userService: UserService) {
    }

    ionViewDidEnter() {
      this.fetchShoppingList();
      
    }

    fetchShoppingList() {
    	this.shoppingProvider.getAll().subscribe(
    		(res: Shopping[]) =>
    			this.shoppingList = res.filter(item => item.client == this.userService.getUser().id)
    			);
    }

    getTotal() {
      return this.shoppingList.reduce((i, j) => i + j.price * j.amount, 0);
    }



}