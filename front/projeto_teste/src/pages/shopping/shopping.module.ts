import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';

import { ShoppingPage } from './shopping';

@NgModule({
    declarations: [
        ShoppingPage,
    ],
    imports: [
        IonicPageModule.forChild(ShoppingPage),
        TranslateModule.forChild()
    ],
    exports: [
        ShoppingPage
    ]
})
export class ShoppingPageModule { }
