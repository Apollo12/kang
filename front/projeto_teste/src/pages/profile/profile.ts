import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Component } from '@angular/core';

/**
 * The Settings page is a simple form that syncs with a Settings provider
 * to enable the user to customize settings for the app.
 *
 */
@IonicPage()
@Component({
    selector: 'page-profile',
    templateUrl: 'profile.html'
})
export class ProfilePage {
    _user: any;

    constructor(public navCtrl: NavController, public navParams: NavParams) { }

    // editRegister() {
    //     this.navCtrl.push('RegisterPage');
    // }

    showCart() {
        this.navCtrl.push('CartPage');
    }

    showShopping() {
        this.navCtrl.push('ShoppingPage');
    }

    verifyWeather() {
        this.navCtrl.push('WeatherPage');
    }

    showExchange() {
        this.navCtrl.push('ExchangePage');
    }

    showCalculator() {
        this.navCtrl.push('CalculatorPage');
    }

    doLogout() {
        console.log("Logout");
        this.navCtrl.parent.parent.setRoot('WelcomePage');
    }
}
