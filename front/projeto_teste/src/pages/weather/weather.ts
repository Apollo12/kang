import { IonicPage, NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';
import { Component } from '@angular/core';

import { Weather } from '../../models/weather';
import { Weather_Main } from '../../models/weather';
import { Weather_Wind } from '../../models/weather';
import { WeatherResponse } from '../../models/weather';
import { Cities } from '../../models/cities';
import { WeatherProvider } from '../../providers/weather/weather';
import { CitiesProvider } from '../../providers/cities/cities';

/**
 * The Settings page is a simple form that syncs with a Settings provider
 * to enable the user to customize settings for the app.
 *
 */
@IonicPage()
@Component({
    selector: 'page-weather',
    templateUrl: 'weather.html'
})
export class WeatherPage {
    private clima: WeatherResponse;
    shownGroup = null;
    latitude: number;
    longitude: number;
    cities: Cities[];
    fullAddress: string;

    constructor(public navCtrl: NavController, public navParams: NavParams,  public loadingController: LoadingController,
        public weatherProvider: WeatherProvider,
        public citiesProvider: CitiesProvider,
        public toastCtrl: ToastController) {
        console.log('WeatherPage.ts');
    }

    ionViewWillEnter() {
        this.citiesProvider.getCities()
            .subscribe((res: Cities[]) => this.cities = res);
    }

    psgCityItems: any = [];

    getItemsCidadePsg(ev) {
        let val = ev.target.value;
        if (!val || !val.trim()) {
            this.psgCityItems = [];
            return;
        }
        this.psgCityItems = this.cities.filter(city => city.fullAddress.toUpperCase().includes(val.toUpperCase()));
    }

    toggleGroup(group) {
        if (this.isGroupShown(group)) {
            this.shownGroup = null;
        } else {
            this.shownGroup = group;
        }
    };

    isGroupShown(group) {
        return this.shownGroup === group;
    };

    convertLatitude(latitude) {
        let city = this.cities.find(city => city.fullAddress === latitude);
        return city.lat;
    }

    convertLongitude(longitude) {
        let city = this.cities.find(city => city.fullAddress === longitude);
        return city.lng;
    }

    async search_weather() {
        if (this.fullAddress == null) {
            let toast = this.toastCtrl.create({
              message: "Erro o campo LOCAL esta vazio, por favor preencha o campo antes de continuar",
              duration: 3000,
              position: 'top'
            });
            toast.present();
          } 
        else{
            const loading = this.loadingController.create({
                cssClass: 'my-custom-class',
                content: 'Aguarde...'
            });
            await loading.present();
            let latitude = this.convertLatitude(this.fullAddress);
            let longitude = this.convertLongitude(this.fullAddress);
            this.weatherProvider.getWeather(latitude, longitude).
                subscribe((res: WeatherResponse) =>{
                    this.clima = res;
                    console.log(this.clima);
                    loading.dismiss();
                });
        }
    }

    isWeather() {
        if (this.clima == null) {
            return false;
        }
        return true;
    }

}