import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, ToastController, LoadingController } from 'ionic-angular';

import { CartService } from '../../services/cart.service';
import { BehaviorSubject } from 'rxjs';

import { hoteisProvider} from '../../providers/hotel/hotel'
import { Product } from '../../models/product';
import { SearchService } from '../../services/search.service';
import { isEmpty } from 'rxjs/operator/isEmpty';


@IonicPage()
@Component({
    selector: 'page-results-hotel',
    templateUrl: 'results-hotel.html'
})
export class ResultsHoteisPage implements OnInit {
    private hoteis: Product[];
    private cart = [];
    private products = [];
    private cartItemCount: BehaviorSubject<number>;
    private respostaVazia = 2;


    constructor(
        public navCtrl: NavController, private toastCtrl : ToastController, public loadingController: LoadingController,
        private cartService : CartService, private hoteisProvider : hoteisProvider,  private searchService: SearchService) {

        console.log('Hello results-hotel.ts');
    }

    ngOnInit() {
        this.cart = this.cartService.getCart();
        this.cartItemCount = this.cartService.getCartItemCount();
        this.hoteis = [];
        this.searchHoteis(this.searchService.getDestination(),this.searchService.getCheckinDate(),this.searchService.getCheckOutDate(),
                            this.searchService.getAdults(), this.searchService.getPrice());
    }


    ionViewDidEnter() {
   
    //this.page = 1;
    //this.infiniteScroll.enable(true);
    //this.getAllUsers(this.page);
    }

    
    addToCart(product) {
    this.cartService.addProduct(product);
    let toast = this.toastCtrl.create({
      message: "O hotel foi adicionado ao carrinho",
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
    }

    openCart() {
    this.navCtrl.push('CartPage');
    }

    async searchHoteis(destino, checkin, checkout, adults, precos) {
        const loading = this.loadingController.create({
            cssClass: 'my-custom-class',
            content: 'Aguarde...'
        });
        await loading.present();
        console.log("chamando a pesquisa no provider")
        this.hoteisProvider.getHoteis(destino, checkin, checkout, adults, precos).
        	subscribe((res: Product[]) =>{
                 this.hoteis = res;
                 console.log("Resposta Final: ");
                 console.log(this.hoteis);
                 loading.dismiss();
                 if (this.hoteis.length == 0){
                    console.log("resposta veio vazia");
                    this.respostaVazia = 1;
                }
                else{
                    this.respostaVazia = 0;
                }    
            });
        
    
        

    }
    
    // isResult(){
    //     if(this.hoteis == null){
    //         console.log("resposta veio nula");
    //         return 2;
    //     }
    //     else if (this.hoteis.length == 0){
    //         console.log("resposta veio vazia");
    //         return 1;
    //     }
    //     else{
    //         return 0;
    //     }
    // }

}


