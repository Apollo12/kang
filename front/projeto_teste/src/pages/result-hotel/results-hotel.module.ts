import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ResultsHoteisPage } from './results-hotel';

@NgModule({
    declarations: [
        ResultsHoteisPage,
    ],
    imports: [
        IonicPageModule.forChild(ResultsHoteisPage),
    ],
})
export class ResultsHoteisPageModule { }
