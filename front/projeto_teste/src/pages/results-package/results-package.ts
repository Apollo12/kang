import { Component } from '@angular/core';
import { IonicPage, NavController, ToastController, LoadingController } from 'ionic-angular';

import { CartService } from '../../services/cart.service';
import { BehaviorSubject } from 'rxjs';

import { PackagesProvider} from '../../providers/packages/packages';
import { Product } from '../../models/product';
import { Package } from '../../models/package';
import { SearchService } from '../../services/search.service';


@IonicPage()
@Component({
  selector: 'page-results-package',
  templateUrl: 'results-package.html',
})
export class ResultsPackagePage {

	  private packages: Package[];
	  private cart = [];
	  private cartItemCount: BehaviorSubject<number>;
	  private respostaVazia = 2;

	  constructor(
	      public navCtrl: NavController, private toastCtrl : ToastController,  public loadingController: LoadingController,
	      private cartService : CartService, private packagesProvider : PackagesProvider, private searchService: SearchService) {
	  }

	  ngOnInit() {
	     this.cart = this.cartService.getCart();
		 this.cartItemCount = this.cartService.getCartItemCount();
		 this.packages = [];
	      this.searchPackages(this.searchService.getOrigin(), this.searchService.getDestination(),
	  					this.searchService.getDeparture(), this.searchService.getMaxValue(), this.searchService.getReturnDate());
	  }

	  ionViewDidEnter() {
	      

	}

	  addToCart(p) {
	      this.cartService.addProduct(p.flight);
	      this.cartService.addProduct(p.hotel);
	      let toast = this.toastCtrl.create({
	        message: "O pacote foi adicionado ao carrinho",
	        duration: 3000,
	        position: 'bottom'
	      });
	      toast.present();
	  }

	  openCart() {
	  this.navCtrl.push('CartPage');
	  }

	  async searchPackages(origin, destination, checkin, maxValue, checkout) {
		const loading = this.loadingController.create({
            cssClass: 'my-custom-class',
            content: 'Aguarde...'
		});
		await loading.present();
	  	this.packagesProvider.getPackages(origin, destination, checkin, maxValue, checkout).
	  	  subscribe((res: Package[]) =>{
				   this.packages = res;
				   loading.dismiss();
                 if (this.packages.length == 0){
                    console.log("resposta veio vazia");
                    this.respostaVazia = 1;
                }
                else{
                    this.respostaVazia = 0;
                }    
			});
	  }



}
