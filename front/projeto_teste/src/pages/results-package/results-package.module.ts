import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ResultsPackagePage } from './results-package';

@NgModule({
  declarations: [
    ResultsPackagePage,
  ],
  imports: [
    IonicPageModule.forChild(ResultsPackagePage),
  ],
})
export class ResultsPackagePageModule {}
